# Agrippa
### A helper for ACF fields

If you're curious, it was named for Marcus Agrippa, Emperor Agusutus's long-time friend, general, and right-hand man.

## Dependencies
- WordPress
- Advanced Custom Fields

## Purpose

Dealing with multiple ACF fields on a page can be a bit of a pain in the butt, so Agrippa is here to help. It grabs fields and their data and wraps them in a handy object, so you can access them as object properties. You can customize which fields are loaded, or just grab everything associated with an ID.

## The Future

As Agrippa gets more use and tool possibilities are unconvered, I'll be adding simple methods to help with manipulating fields. Already on the docket is something to help with looping through repeater fields.

## Usage

The easiest way to install Agrippa is just to add it to your `composer.json` and use Composer to manage it as a dependency. If that doesn't do it for you, though, you can always just manually include it.

*Reminder:* If it's not working, check to see that you you've added `use Livy\Agrippa;` to the top of your PHP file (or that you're writting your class calls as `Livy\Agrippa`).

### Basic

The most basic implementation of Agrippa is just to call it like so:

```php
$fields = new Agrippa;
$fields->loadData();
```

This creates a new Agrippa instance in the $fields variable, and then loads all the fields associated with the current page/post/whatever. To see what fields its added, just call `$fields->getFields()`. This will return an array of the field keys, with their name and label. If you pass bool `true` to that method, it will print out a definition list of the same information.

You can access any field by its name, as you would with any object property, i.e. `$fields->test_field`. Since the only public objects on an Agrippa object are the fields, you can also iterate over them as you would with an array.

### Different IDs

If you would like to get the fields for a post object other than the one you're currently on, you can pass the ID to the constructor when instantiating your Agrippa object:

```php
$fields = new Agrippa( 23 );
$fields->loadData();
```

This will load all the fields from the post object with the id of `23`, but will not load fields from the current post object.

### Adding More Fields

You can add additional fields to your Agrippa object by calling `loadData()` on the object again and passing IDs to it. For instance:

```php
$fields = new Agrippa;
$fields->loadData()->loadData( 23 );
```

This will load all fields from the current post object, as well as all fields from the post object with the ID of `23`.

### Adding Specific Additional Fields

Adding more fields by calling `loadData()` with additional IDs is helpful, but it loads _all_ fields associated with that ID. Maybe you only want to load one, or some of the fields. To do that, simple call `loadData()` with an additional parameter: An array containing a list of the fields you wish to retrieve.

```php
$fields = new Agrippa;
$fields->loadData( 23, [ 'field_572bf3d29d457', 'field_572bf3e19d458' ]);
```

This will load field data for fields `field_572bf3d29d457` and `field_572bf3e19d458` from the post object with ID `23`.

*Notes:*

- To retrieve specific fields from the current object, just pass `null` as the id, like so: `$fields->loadData( null, [ 'field_572bf3d29d457', 'field_572bf3e19d458' ] )`.
- If you attempt to load data from a field that does not exist on that id, it will not add the data, but will not provide a warning that no data exists. In general you should only attempt to retrieve fields that you know will exist on the post object.