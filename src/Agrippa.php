<?php namespace Livy;
    /**
     * Class for manipulating fields
     */
    class Agrippa {

        protected $id;
        private $fields;

        function __construct( $id = null ) {
            if ( !class_exists('WP_Query' ) ) : throw new \Exception("WordPress hasn't been installed."); endif;
            if ( !class_exists('acf') ) : throw new \Exception("ACF hasn't been loaded."); endif;

            if ( $id === null ) :
                $this->setID( $id );
            endif;
        }

        /**
         * Load data into Agrippa. Can be called multiple times.
         * @param int|string $id The ID being queried for fields. Defaults to current page. 
         * Must be an int or the string `option`.
         * @param array $fieldList An array list of fields to be pulled. Will be queried against
         * $id. 
         * @return object $this Instance of Agrippa, for method chaining.
         */
        public function loadData ( $id = null, $fieldList = false )
        {
            if ( $id === null || $id == $this->id ) :
                $id = $this->id;
            elseif ( $this->checkID( $id ) ) :
                $id = $this->checkID( $id );
            else :
                throw new \Exception("This ID doesn't work.");
            endif;

            if ( $fieldList === false ) :
                $fields = get_field_objects( $id );
            elseif ( is_array($fieldList) ) :
                foreach ($fieldList as $field) :
                    if( get_field( $field, $id ) ) :
                        $fields[$field] = get_field_object( $field, $id );
                    else :
                        continue;
                    endif;
                endforeach;
            else :
                throw new \Exception("Bad input.");
            endif;

            foreach ( $fields as $key => $field ) :
                $this->fields[$field['key']] = ['name' => $field['name'], 'label' => $field['label']];
                $fieldObj = new Marcus;
                $fieldObj->type = $field['type'];
                $fieldObj->value = $field['value'];
                if ( isset($field['sub_fields']) ) :
                    $valueArray = [];
                    foreach ( $field['value'] as $valueKey => $row ) :
                        $rowArray = [];
                        foreach ( $row as $rowKey => $rowValue) :
                            $item = new Marcus;
                            $item->setValue( $rowValue );
                            $item->setType( 'subfield' );
                            $rowArray[$rowKey] = $item;
                        endforeach;
                        $valueArray[] = $rowArray;
                    endforeach;
                    $fieldObj->value = $valueArray;                    
                endif;
                $this->{$key} = $fieldObj;
            endforeach;
            return $this;
        }

        /**
         * Determines if the passed $id fits our requirements.
         * @param int|string $id 
         * @return int|boolean $return Returns $id if it passes, bool false if not.
         */
        public function checkID ( $id )
        {
            if ( isset($id) && is_numeric( $id ) || $id === 'option' ) :
                return $id;
            else :
                return false;
            endif;
        }

        /**
         * Sets private property `id`.
         * @param int|string $id
         * @return object $this
         */
        public function setID ( $id )
        {
            $this->id = $this->checkID( $id );
            return $this;
        }

        /**
         * Returns a list of the fields available on this Agrippa.
         * @param bool $formatted Whether or not to return a formatted list.
         * @return array|void $return If $formatted is bool false, then it returns $this->fields
         * as an unformatted array. If $formatted is bool true, then it returns a definition list
         * with all the fields.
         */
        public function getFields( $formatted = false )
        {
            if( $formatted === false ) :
                return $this->fields;
            elseif( $formatted === true ) :
                echo "<dl>";
                foreach ($this->fields as $key => $value) :
                    echo "<dt>$key</dt>";
                    foreach ($value as $key => $value) :
                        echo "<dd>$key : $value</dd>";
                    endforeach;
                endforeach;
                echo "</dl>";
            endif;
        }

    }

    /**
     * Class wrapper for fields
     */
    class Marcus extends Agrippa {

        public $uid;
        public $type;
        public $value;

        function __construct() 
        {
            $this->uid = uniqid( 'marcus' );
        }

        public function setName ( $name )
        {
             $this->name = $name;
        }

        public function setType ( $type )
        {
             $this->type = $type;
        }

        public function setValue ( $value )
        {
             $this->value = $value;
        }

        public function startLoop ()
        {
            ob_start();
        }

        public function endLoop ()
        {
            return ob_get_clean();
        }
    }

